import Vue from "vue";
import App from "./App.vue";
import MyButton from "./components/MyButton";

Vue.component("my-button", MyButton);

new Vue({
  el: "#app",
  render: h => h(App)
});
